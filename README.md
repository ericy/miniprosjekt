# Miniprosjekt - Systemutvikling 2

## How to install and run project:
From root directory

### Client
```
cd klient
npm install
npm test
npm start
```

### Server
```
cd server
npm install
npm test
npm start
```

### Socket server IO
```
cd socketio
npm install
npm start
```

# Open web application:
- http://localhost:3000

Sidenote: Flow typechecking is used, to check write ```flow``` while in server or klient directory.

### Screenshots:
![Screenshot_2019-11-24_at_23.16.07](/uploads/2a7fa43a44d664a13875054042eca461/Screenshot_2019-11-24_at_23.16.07.png)
![Screenshot_2019-11-24_at_23.17.01](/uploads/48af983b93e7ca7f3b4cd610daf828e0/Screenshot_2019-11-24_at_23.17.01.png)
![Screenshot_2019-11-24_at_23.18.01](/uploads/c0960d584ada400633636186a21ba57b/Screenshot_2019-11-24_at_23.18.01.png)